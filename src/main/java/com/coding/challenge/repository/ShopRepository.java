package com.coding.challenge.repository;

import com.coding.challenge.model.Shop;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface ShopRepository extends JpaRepository<Shop, Long> {
    String FETCH_NON_LIKED_SHOPS = "SELECT * FROM coding_challenge.shop where coding_challenge.shop.shop_id NOT " +
            "IN (SELECT coding_challenge.liked_shop.shopID FROM coding_challenge.liked_shop)";

    @Query(value = FETCH_NON_LIKED_SHOPS, nativeQuery = true)
    List<Shop> shops();
}