package com.coding.challenge.repository.liquibase;

import java.sql.SQLException;

import javax.sql.DataSource;

import liquibase.Contexts;
import liquibase.Liquibase;
import liquibase.database.jvm.JdbcConnection;
import liquibase.exception.DatabaseException;
import liquibase.exception.LiquibaseException;
import liquibase.resource.ClassLoaderResourceAccessor;

public final class LiquibaseRunner {

    private static final String LIQUIBASE_CHANGELOG = "liquibase/changelog.yaml";

    private LiquibaseRunner() {
        // utility class
    }

    public static void updateSchema(DataSource ds) throws LiquibaseException {

        Liquibase liquibase = null;
        try {
            liquibase = new Liquibase(LIQUIBASE_CHANGELOG,
                new ClassLoaderResourceAccessor(),
                new JdbcConnection(ds.getConnection()));
            liquibase.update(new Contexts());
        } catch (SQLException e) {
            throw new DatabaseException(e);
        } finally {
            if (null != liquibase && null != liquibase.getDatabase()) {
                liquibase.getDatabase().close();
            }
        }
    }
}