package com.coding.challenge.ressource;

import static com.coding.challenge.response.Message.EMAIL_ALREADY_EXIST;
import static com.coding.challenge.response.Message.REGISTERED_SUCCESSFULLY;

import javax.validation.Valid;

import com.coding.challenge.core.ShopService;
import com.coding.challenge.dto.ShopDto;
import com.coding.challenge.model.Location;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.coding.challenge.dto.LoginDto;
import com.coding.challenge.dto.RegistrationDto;
import com.coding.challenge.core.UserService;
import com.coding.challenge.model.User;
import com.coding.challenge.response.JwtResponse;
import com.coding.challenge.security.auth.TokenProvider;

@CrossOrigin(origins = "*", maxAge = 3600)
@RestController
@RequestMapping("/api/shop")
public class ShopApi {

    @Autowired
    private AuthenticationManager authenticationManager;

    @Autowired
    private UserService userService;

    @Autowired
    private TokenProvider tokenProvider;

    @Autowired
    private ShopService shopService;

    @PostMapping("/signin")
    public ResponseEntity<?> authenticateUser(@Valid @RequestBody LoginDto loginDto) {
        Authentication authentication = authenticationManager.authenticate(
            new UsernamePasswordAuthenticationToken(loginDto.getEmail(), loginDto.getPassword()));

        SecurityContextHolder.getContext().setAuthentication(authentication);

        String jwt = tokenProvider.generateJwtToken(authentication);
        UserDetails userDetails = (UserDetails) authentication.getPrincipal();

        return ResponseEntity.ok(new JwtResponse(jwt, userDetails.getUsername(), userDetails.getAuthorities()));
    }

    @PostMapping("/signup")
    public ResponseEntity<?> registerUser(@Valid @RequestBody RegistrationDto registrationDto) {
        User user = new User(registrationDto.getEmail(), registrationDto.getPassword());
        if (!userService.registerUser(user)) {
            return new ResponseEntity<>(EMAIL_ALREADY_EXIST.value(), HttpStatus.BAD_REQUEST);
        }
        return new ResponseEntity<>(REGISTERED_SUCCESSFULLY.value(), HttpStatus.OK);
    }

    @PostMapping("/shops")
    public ResponseEntity<?> shops(@RequestBody Location location) {
        return ResponseEntity.ok(shopService.sortedShops(location));
    }

    @PostMapping("/like")
    public ResponseEntity<?> like(@Valid @RequestBody ShopDto shop) {
        Authentication authentication =  SecurityContextHolder.getContext().getAuthentication();
        UserDetails userDetails = (UserDetails) authentication.getPrincipal();
        String email = userDetails.getUsername();
        userService.like(shop, email);
        return ResponseEntity.ok(email);
    }
}