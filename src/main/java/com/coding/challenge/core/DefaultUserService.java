package com.coding.challenge.core;

import com.coding.challenge.dto.ShopDto;
import com.coding.challenge.model.Shop;
import com.coding.challenge.model.User;
import com.coding.challenge.repository.UserRepository;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.ArrayList;
import java.util.List;

@Service
@Slf4j
public class DefaultUserService implements UserService {

    @Autowired
    private UserRepository userRepository;

    @Autowired
    private ShopService shopService;

    @Autowired
    private PasswordEncoder encoder;

    @Override
    public boolean registerUser(User newUser) {
        if (userRepository.existsByEmail(newUser.getEmail())) {
            return false;
        } else {
            User user = new User(newUser.getEmail(), encoder.encode(newUser.getPassword()));
            userRepository.save(user);
            log.info("User saved correctly");
            return true;
        }
    }

    @Override
    @Transactional
    public void like(ShopDto dto, String email) {
        Shop shop = shopService.loadShopByID(dto.getShopID())
                .orElseThrow(() -> new BusinessException("Shop not found"));
        User user = userRepository.findByEmail(email)
                .orElseThrow(() -> new BusinessException("User not found"));
        List<Shop> likedShops = new ArrayList<>();
        likedShops.add(shop);
        user.setLikedShops(likedShops);
        userRepository.save(user);
    }
}
