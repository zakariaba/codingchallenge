package com.coding.challenge.core;

import com.coding.challenge.dto.ShopDto;
import com.coding.challenge.model.User;

public interface UserService {

    boolean registerUser(User newUser);

    void like(ShopDto shop, String email);
}
