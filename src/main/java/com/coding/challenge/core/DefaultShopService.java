package com.coding.challenge.core;

import static com.coding.challenge.core.DistanceCalculator.distance;
import static java.util.stream.Collectors.toList;

import com.coding.challenge.dto.ShopDto;
import com.coding.challenge.model.Location;
import com.coding.challenge.model.Shop;
import com.coding.challenge.repository.ShopRepository;
import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;

@Service
public class DefaultShopService implements ShopService {

    @Autowired
    private ShopRepository shopRepository;

    @Autowired
    private ModelMapper modelMapper;

    @Override
    public List<ShopDto> sortedShops(Location location) {
        List<Shop> sortedShops = shopList();
        sortedShops.sort((Shop s1, Shop s2) ->
                (int) (distance(location, s1.getLocation()) - distance(location, s2.getLocation())));

        return toDTOList(sortedShops);
    }

    @Override
    public List<Shop> shopList() {
        return shopRepository.shops();
    }

    @Override
    public Optional<Shop> loadShopByID(long id) {
        return shopRepository.findById(id);
    }

    private List<ShopDto> toDTOList(List<Shop> shops) {
        return shops.stream()
                .map(shop -> convertToDTO(shop))
                .collect(toList());
    }

    private ShopDto convertToDTO(Shop shop) {
        return modelMapper.map(shop, ShopDto.class);
    }
}
