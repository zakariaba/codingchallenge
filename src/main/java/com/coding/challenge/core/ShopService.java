package com.coding.challenge.core;

import com.coding.challenge.dto.ShopDto;
import com.coding.challenge.model.Location;
import com.coding.challenge.model.Shop;

import java.util.List;
import java.util.Optional;

public interface ShopService {
    List<ShopDto> sortedShops(Location location);

    List<Shop> shopList();

    Optional<Shop> loadShopByID(long id);
}
