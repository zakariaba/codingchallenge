package com.coding.challenge.core;

import com.coding.challenge.model.Location;

import static java.lang.Math.acos;
import static java.lang.Math.cos;
import static java.lang.Math.sin;
import static java.lang.Math.PI;

class DistanceCalculator {

    public static double distance(Location location, Location loc) {
        double theta = location.getLongitude() - loc.getLongitude();
        double locationRad = deg2rad(location.getLatittude());
        double secondLocRad = deg2rad(loc.getLatittude());
        double dist = sin(locationRad) * sin(secondLocRad) + cos(locationRad) * cos(secondLocRad) * cos(deg2rad(theta));
        dist = acos(dist);
        dist = rad2deg(dist);
        dist = dist * 60 * 1.1515;
        dist = dist * 1.609344;
        return dist;
    }

    private static double deg2rad(double deg) {
        return (deg * PI / 180.0);
    }

    private static double rad2deg(double rad) {
        return (rad * 180.0 / PI);
    }
}
