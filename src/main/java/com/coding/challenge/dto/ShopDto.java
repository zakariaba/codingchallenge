package com.coding.challenge.dto;

import com.coding.challenge.model.Location;
import com.coding.challenge.model.User;
import com.fasterxml.jackson.annotation.JsonIgnore;
import lombok.Getter;
import lombok.Setter;

import java.util.List;

public class ShopDto {

    @Getter
    @Setter
    private long shopID;
    @Getter
    @Setter
    private String name;
    @Getter
    @Setter
    private Location location;
    @Getter
    @JsonIgnore
    private List<User> users;

    public ShopDto() {
    }

    public ShopDto(long shopID, String name, Location location, List<User> users) {
        this.shopID = shopID;
        this.name = name;
        this.users = users;
    }
}
