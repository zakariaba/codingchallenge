package com.coding.challenge.dto;

import lombok.Getter;

public class LikeDto {

    @Getter
    private int userID;
    @Getter
    private int shopID;

    public LikeDto(int userID, int shopID) {
        this.userID = userID;
        this.shopID = shopID;
    }
}
