package com.coding.challenge.dto;

import lombok.Getter;

import javax.validation.constraints.Email;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.Size;

public class LoginDto {

    @Email
    @Getter
    @NotBlank
    @Size(max = 60)
    private String email;

    @NotBlank
    @Getter
    @Size(min = 6, max = 40)
    private String password;
}