package com.coding.challenge.model;

import lombok.Getter;

import javax.persistence.Embeddable;

@Embeddable
public class Location {

    @Getter
    private double latittude;
    @Getter
    private double longitude;

    public Location() {
    }

    public Location(double latittude, double longitude) {
        this.latittude = latittude;
        this.longitude = longitude;
    }
}
