package com.coding.challenge.model;

import lombok.Getter;
import lombok.Setter;

import javax.persistence.*;
import java.util.List;

@Entity
@Table(name = "shop")
public class Shop {

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    @Column(name = "shop_id")
    @Getter
    @Setter
    private Long id;

    @Getter
    @Setter
    private String name;

    @Embedded
    @Getter
    @Setter
    private Location location;

    @ManyToMany(fetch = FetchType.EAGER, mappedBy = "likedShops")
    @Getter
    @Setter
    private List<User> users;

    public Shop() {
    }

    public Shop(String name, Location location, List<User> userList) {
        this.name = name;
        this.location = location;
        this.users = userList;
    }

    public Shop(String name) {
        this.name = name;
    }
}
