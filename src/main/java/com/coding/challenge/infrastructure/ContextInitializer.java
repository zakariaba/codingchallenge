package com.coding.challenge.infrastructure;

import javax.annotation.Resource;
import javax.servlet.ServletContext;
import javax.servlet.ServletException;
import javax.sql.DataSource;

import org.springframework.boot.web.servlet.ServletContextInitializer;
import org.springframework.context.annotation.Configuration;

import com.coding.challenge.repository.liquibase.LiquibaseRunner;

import liquibase.exception.LiquibaseException;

@Configuration
public class ContextInitializer implements ServletContextInitializer {

    @Resource
    private DataSource dataSource;

    @Override
    public void onStartup(ServletContext servletContext) throws ServletException {
        try {
            LiquibaseRunner.updateSchema(dataSource);
        } catch (LiquibaseException e) {
            throw new IllegalStateException(e);
        }
    }
}
