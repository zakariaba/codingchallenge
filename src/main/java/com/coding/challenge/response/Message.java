package com.coding.challenge.response;

public enum Message {
    REGISTERED_SUCCESSFULLY("User registered successfully"),
    EMAIL_ALREADY_EXIST("Email already in use");

    private String message;

    Message(String message) {
        this.message = message;
    }

    public String value() {
        return message;
    }
}
