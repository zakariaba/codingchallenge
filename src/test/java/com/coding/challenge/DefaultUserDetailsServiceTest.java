package com.coding.challenge;

import static java.util.Arrays.asList;
import static java.util.Collections.emptyList;
import static java.util.Optional.of;
import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertAll;
import static org.junit.jupiter.api.Assertions.assertIterableEquals;
import static org.junit.jupiter.api.Assertions.assertThrows;
import static org.mockito.Mockito.lenient;

import com.coding.challenge.model.User;
import com.coding.challenge.repository.UserRepository;
import com.coding.challenge.security.services.DefaultUserDetailsService;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UsernameNotFoundException;

@ExtendWith(MockitoExtension.class)
@DisplayName("Tests User detail service")
public class DefaultUserDetailsServiceTest {

    @Mock
    UserRepository userRepository;

    @InjectMocks
    DefaultUserDetailsService userDetailsService;

    @BeforeEach
    void setUp() {
        User user = new User("test@yopmail.com", "pass", emptyList());
        lenient().when(userRepository.findByEmail("test@yopmail.com")).thenReturn(of(user));
    }

    @Test
    @DisplayName("Should return User detail if user exist")
    void shouldLoadUser() {
        UserDetails userDetails = userDetailsService.loadUserByUsername("test@yopmail.com");
        assertAll(
                () -> assertEquals(userDetails.getUsername(), "test@yopmail.com"),
                () -> assertEquals(userDetails.getAuthorities().size(), 1),
                () -> assertIterableEquals(userDetails.getAuthorities(), asList(new SimpleGrantedAuthority("USER")))
        );
    }

    @Test
    @DisplayName("Should throw exception if user does not exist")
    void shouldThrowException() {
        assertThrows(UsernameNotFoundException.class,
                () -> userDetailsService.loadUserByUsername("testNA@yopmail.com")
        );
    }
}
