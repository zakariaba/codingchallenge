package com.coding.challenge;

import static java.util.Collections.emptyList;
import static org.junit.jupiter.api.Assertions.assertFalse;
import static org.junit.jupiter.api.Assertions.assertTrue;
import static org.mockito.Mockito.lenient;

import com.coding.challenge.core.DefaultUserService;
import com.coding.challenge.core.ShopService;
import com.coding.challenge.model.User;
import com.coding.challenge.repository.UserRepository;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Nested;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;
import org.springframework.security.crypto.password.PasswordEncoder;

@ExtendWith(MockitoExtension.class)
public class DefaultUserServiceTest {

    @Mock
    UserRepository userRepository;

    @Mock
    private ShopService shopService;

    @Mock
    private PasswordEncoder encoder;

    @InjectMocks
    DefaultUserService userService;


    @Nested
    @DisplayName("Test user registration")
    class UserRegistration {

        @Test
        @DisplayName("User Registration Successful")
        void shouldRegisterUser() {
            lenient().when(userRepository.existsByEmail("test@yopmail.com")).thenReturn(false);
            User user = new User("test@yopmail.com", "pass", emptyList());
            boolean registrationStatus = userService.registerUser(user);
            assertTrue(registrationStatus);
        }

        @Test
        @DisplayName("User Registration fail")
        void shouldFailWhileRegisterUser() {
            lenient().when(userRepository.existsByEmail("test@yopmail.com")).thenReturn(true);
            User user = new User("test@yopmail.com", "pass", emptyList());
            boolean registrationStatus = userService.registerUser(user);
            assertFalse(registrationStatus);
        }
    }
}
