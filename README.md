Backen API
==============

Technologies : 
Java : 1.8
Spring boot 2.0.5

Project info : 

Run with Maven :
	1 - mvn clean install
	2 - Run mvn spring-boot:run

Description : 

Registration endpoint : http://localhost:8787/api/shop/signup type POST
Loging endpoint : http://localhost:8787/api/shop/signin type POST
Like endpoint : http://localhost:8787/api/shop/like type POST
Shop list endpoint : http://localhost:8787/api/shop/shops type GET

The API is using liquibase for database migration, so you do not need to create any table just provide the database properties and liquibase will create schema.




